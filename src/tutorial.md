# Tutorial

Let's create a token pair on Pancake and change the pool price.

The price in the pair is defined by the amount of tokens in each token pool. The token pool balance and therefore the price is changed by trades. Each trade requires a number of tokens according to the pricing curve, and so to change the price of a large pool, a large amount of liquidity is needed for a trade. A large liquidity provider, however, can withdraw liquidity from both token pools to reduce the slippage. Calibrator removes maximum liquidity from the pools, performs trades necessary to shift the price, and returns the liquidity back to the pools, sending all remaining tokens back to the liquidity provider. 

When you create a new pool, Pancake deploys a new Pair contract. You can find its address in the logs for the pool creation in your transaction history at the [explorer](https://bscscan.info). Paste the address of a Pancake Pair contract into the form in the calibrator [website](calibrator.norcivilianlabs.org).

> The fee size for Pancake is set by default and doesn't need changing. To find the fee size for a different marketplace, see [Finding the Fee Size](./01_finding_the_fee_size.md).

Set the desired price in the pool and transfer boundaries, then press "Calibrate". You will be asked to sign two transactions - for the transfer of LP tokens needed for price change, and for the calibration transaction. If the network state changes drastically before calibration and moves outside the transfer boundaries, the transaction will fail and your LP balance won't change.

To learn more about calibration see the [User Guides](./user_guides.md).

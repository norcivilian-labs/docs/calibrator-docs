# Getting Started

Open the [website](calibrator.norcivilianlabs.org), connect to Metamask and choose the network.

> If your network is not listed in the [deployments](./deployments.md), see [Deploying Calibrator](./02_deploying_calibrator.md).

Paste the address of a token pair and the default amm fee.

> Each UniswapV2 clone has a different fee size, see [Finding the Fee Size](./01_finding_the_fee_size.md).

Set the desired price in the pool and verify the estimation. Set the boundaries of a transfer in case the network state changes before calibration. 

Press "Calibrate". Sign the transaction to approve the transfer of LP tokens, and then sign the transaction to calibrate.

See [Tutorial](./tutorial.md) to deploy a pool from scratch, and [User Guides](./user_guides.md) to learn more about calibration.

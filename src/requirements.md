# Requirements

- user must set given price in a Uniswap v2 pool
- user must estimate amount of LP tokens required for a given price change
- user must estimate fee losses incurred by a given price change
- user must specify a budget to fail transaction if the network state changes

# Design

nascent pool calibration smart contract

competes: arbitrage bot smart contracts

interacts: externally owned blockchain accounts, Uniswap Pair smart contract

constitutes: smart contract

includes: estimator, calibrator, settings

resembles: Uniswap v2 Router contract

patterns: factory

stakeholders: fetsorn

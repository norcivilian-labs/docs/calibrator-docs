# calibrator-docs

Documentation for [calibrator](https://gitlab.com/norcivilian-labs/calibrator), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
